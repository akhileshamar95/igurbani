import React, { Component } from 'react';
import '../components/allpagecss/homepage.css'
import igurbanilogo from '../assets/images/logo.png';
import personicon from '../assets/images/personicon.png';
import arrowicon from '../assets/images/arrowicon.png';
import overviewicon from '../assets/images/overview.png';
import usericon from '../assets/images/users.png';
import bookicon from '../assets/images/book.png';
import contribute from '../assets/images/contribute.png'
import updateicon from '../assets/images/update.png'
import notification from '../assets/images/notification.png'



class home extends Component{
    render(){
      return(
        <div className="topnav">
        <a className="active" href="#igurbanilogo" ><img className="img-class1" src={igurbanilogo} alt="igurbanilogo" /></a>
        <span className="igurbanitext"><span classname="semi">iGurbani</span></span>
        <span className="apartofyou">A  part  of  you</span>
       <div className="topnav-right">
       <a className="active" href="#jjvjv" ><img className="img-class2" src={personicon} alt="personicon" /></a>
       <a className="active" href="#uyvv" ><img className="arrow" src={arrowicon} alt="arrowicon" /></a>
       </div>
         {/* sidebar start**/}
       <div class="sidenav">
       <div className="overview">
       <a className="active" href="#overviewicon" ><img className="overviewicon" src={overviewicon} alt="overviewicon" /></a>
       <span className="overviewtext">Overview</span>
       </div>

       <div className="">
       <a className="active" href="#personviewtext" ><img className="userviewicon" src={usericon} alt="usericon" />
       <span className="personviewtext">Users</span></a>
       </div>
       <div className="">
       <a className="active" href="#bookviewicon" ><img className="bookviewicon" src={bookicon} alt="bookicon" />
       <span className="bookviewtext">Books</span></a>
       </div>

       <div className="">
       <a className="active" href="#Contribute" ><img className="contributeicon" src={contribute} alt="contribute" />
       <span className="contributetext">Contribute</span></a>
       </div>

       <div className="">
       <a className="active" href="#updateicon" ><img className="updateicon" src={updateicon} alt="updateicon" />
       <span className="updatetext">Update</span></a>
       </div>

       <div className="">
       <a className="active" href="#updateicon" ><img className="notification
       " src={notification} alt="updateicon" />
       <span className="notificationtext">Notification</span></a>
       </div>
     </div>
    {/* sidebar close**/}

     {/* main-div-container start**/}
     <div class="main">
     <div className="flex-container">
     {/* div-container start**/}
      <div className="divclass1">
      <span className="totaluser">Total Users</span>
      <span className="divclasscount">500</span>
      </div>
      <div className="divclass2">
      <span className="newuser">New users</span>
      <span className="newusercount">18</span>
      </div>
      <div className="divclass2">
      <span className="newcontribut">Total contributions</span>
      <span className="newusercount">₹5500</span>
      </div>
      <div className="divclass2">
      <span className="newcontribut">New contributions </span>
      <span className="newusercount">₹670</span>
      </div>
      </div>
      {/* div-container close**/}
      {/* div-container month start**/}
      <div className="divmoncontainer">
       <div>
      <div className="">
       <div className="week"><a href="#week">Week </a></div>
       <div className="month"><a href="#month">Month </a></div>
       <div className="year"><a href="#year">Year</a></div>
      </div>
      
      <hr className="hr"></hr>
      <div className="totaluserweek">Total user-Week</div>
      </div>
      <div className=""> 
        <div className="mon"></div>
        <div className="monname">Mon</div>
        <div className="mon12">12</div>
      
       <div className="">
         <div className="tue"></div>
         <div className="tuename">Tue</div>
         <div className="tue8">8</div>
        </div>

        <div className="">
        <div className="wed"></div>
        <div className="wedname">wed</div>
        <div className="wed10">10</div>
        </div>

        <div className="">
        <div className="thu"></div>
        <div className="thuname">Thu</div>
        <div className="thu12">12</div>
        </div>

        <div className="">
        <div className="fri"></div>
        <div className="friname">Fri</div>
        <div className="fri4">4</div>
        </div>

        <div className="">
        <div className="sat"></div>
        <div className="satname">Sat</div>
        <div className="sat6">6</div>
        </div>

        <div className="">
        <div className="sun"></div>
        <div className="sunname">Sun</div>
        <div className="sun11">11</div>
        </div>
        
        

      
      
      </div>
    
     
      </div>
      {/* div-container month close**/}
     
       
  
     


     </div>
     {/* main-dic-container close**/}

       
     

       </div>
     
    )
      }
}
export default home;

